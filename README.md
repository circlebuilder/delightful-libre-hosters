# Delightful Libre Hosters
[![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A list of people and organizations who deploy, maintain and offer open source services to the public.

## [Activism International](https://activism.international/)
> Activism.International provides online tools for secure communication, conferences and more to eco activists for free. In the Activism Cloud, you can write encrypted messages ﹣ even to people who do not use Activism Cloud ﹣, store files, create calendars and plan video conferences etc.

Pricing: 10GB for any eco activist group complying with their usage policy.

Services: Nextcloud, Matrix, BigBlueButton, Jitsi Meet, Wireguard, Etherpad, Owncast, Invidious.

## [Autonomic](https://autonomic.zone)
> Autonomic is a co-operative that is owned and run by its workers. We build technologies and infrastructure to empower users to make a positive impact on the world. All of our services reflect our commitment to our core values: sustainability, transparency, privacy.

Pricing: consultation required.

Services: Wordpress, Rocket.Chat, Netxcloud, Discourse.

## [Cloud68](https://cloud68.co/)
> At Cloud68.co we provide solopreneurs, SMEs and organisations with reliable, safe and ready2use digital infrastructure. We do this by using only open source solutions following our strong belief that YOU should have more control and privacy over your tech infrastructure. Bonus: human support!

Pricing: different per service, some services require you to get a quote or schedule a call with them.

Services: BigBlueButton, Bitwarden, Discourse, DokuWiki, Editoria, Funkwhale, Ghost, GitLab, Invoice Ninja, Kotahi, Wordpress, Matomo, MediaWiki, Metabase, Mattermost, PeerTube, WeKan, Nextcloud, Zammad, WriteFreely.

## [CommonsCloud.coop](https://www.commonscloud.coop/en/)

> CommonsCloud.coop is made up of people and organisations that pool digital cloud resources in a technological and cooperative project with free software; people and organisations responsible for the implementation, maintenance, management and support of the service.

Pricing: different per service and per tier. A 'takeTheLeap' service can be bought to help in a migration.

Services: BigBlueButton (via Meet.coop), Collabora Online, Discourse, Dolibarr, LimeSurvey, NextCloud, Zimbra Collaboration.

## [Dis `root`](https://disroot.org/en)
> Disroot is a platform providing online services based on principles of freedom, privacy, federation and decentralization.

Pricing: free.

Services: Email, Nextcloud, Discourse, XMPP, Etherpad, EtherCalc, PrivateBin, Lufi, Searx, Framadate, Taiga, Jitsi Meet, Gitea, Mumble, Cryptpad. 

## [Indie Hosters](https://indiehosters.net)
> We are a collective of people from diverse backgrounds, endowed with specific skills/networks, who develop a transversal view sensitive to human, ecological, technical, economic, educational, artistic, legal and political issues and driven by the desire to include IndieHosters in the future of commons digital (translated from French).

Pricing: starts at 8EUR per month.

Services: Nextcloud, OnlyOffice, Rocket.Chat, Jitsi Meet.

## [infra4future](https://infra4future.de/):
> Infra4future.de was based on the need of various activist groups in the Munich area to work better together, if possible without joining more and more confusing individual chat groups or working with other usual (proprietary and commercial) platforms such as Slack or similar. This server was built by hacc as a free alternative to such services, to have a platform that is by and for the movement. 

Pricing: free, requires registration.

Services: Nextcloud, Mattermost, Discourse, Mumble, LimeSurvey, GitLab.

## [LibreOps](https://libreops.cc/)
> We believe that building, owning and controlling technology is important. Hackers and Open Source communities have an opportunity and a responsibility to offer an alternative, to offer services and tools that people can trust. Beyond corporate control, that comes with tracking and data farming. Decentralized services and tools that respect users privacy by default.

Pricing: free.

Services: LibreDNS, radicalDNS, Etherpad, Jabber, Konekti, Diskuti, Mastodon, Jitsi Meet, Mumble, Syncthing Relay, Tor Relays.

## [Nomagic](https://nomagic.uk/)
> Nomagic aims to be a trusted partner of your digital life.  To achieve this, we provide a large catalogue of services hosted on our servers. We do not engage in data monetization whatsoever, and believe that people's data and online activity should not be traded in any way. 

Pricing: requires membership of 3 months minimum, they have a Standard and Insecure Situation membership. Offer some free services.

Services: Email (bundle of open source services), Seafile, Lufi, Vaultwarden, Matrix Synapse, Tiny Tiny RSS, Etherpad, HedgeDoc, OnlyOffice, Kanboard, Ejabberd, DokuWiki, WordPress, Static web hosting, TiddlyWiki, Pleroma, GitLab, PeerTube, Funkwhale, Wallabag, Lutim, LSTU, Jitsi Meet, Lime Survey, Sympa, Mobilizon, Framadate, Turtl, PrivateBin, EtherCalc, RSS-Bridge. 

## [OpenDesktop](https://www.opendesktop.org/)
> Opendesktop.org is a libre platform providing free cloud storage, online office editing, contacts & calender tools, personal chat and messaging, as well as project development and product publishing to anyone who values freedom and openness.

Pricing: free, account required.

Services: Products (ocs-webserver), GitLab, Nextcloud, Discourse, Riochat/Matrix, Discourse, OpenStreet

## [Silkky.Cloud](https://www.silkky.cloud/)
> Infrastructure, resources and guides aimed at helping you to protect your privacy against global surveillance.

Pricing: free, no account required.

Services: Piped, Pleroma, Send, Vikunja, Bitwarden, Libreddit, Nitter.

## [The Good Cloud](https://thegood.cloud)
>  Our goal is to provide a secure cloud solution, a platform where you can store all your files with peace of mind. You don’t have to worry about us viewing, analyzing or selling your data. We are proud partners of Nextcloud GmbH.

Pricing: free account with 2GB, charges monthly/yearly for more storage.

Services: Nextcloud, CollaboraOffice, OnlyOffice.

## Groups of Libre Hosters
- [Chatons](https://www.chatons.org): CHATONS – kittens in french – is the Collective of Hosters Alternative, Transparent, Open, Neutral and Solidarity. This collective aims to bring together structures offering free, ethical and decentralised online services in order to allow users to quickly find alternatives that respect their data and privacy to the services by GAFAM (Google, Apple, Facebook, Amazon, Microsoft).
- [librehosters](https://libreho.st/): librehosters is a network of cooperation and solidarity that uses free software to encourage decentralisation through federation and distributed platforms. Our values connect transparency, fairness and privacy with a culture of data portability and public contributions to the commons.

## Maintainers
If you have questions or feedback regarding this list, then please create an [Issue in our tracker](https://codeberg.org/jonatasbaldin/delightful-sustainable-vps/issues), and optionally @mention one or more of our maintainers:
- [@jonatasbaldin](https://codeberg.org/jonatasbaldin)

## Contributors
With delight we present you some of our [delightful contributors](delightful-contributors.md) (please add yourself if you are missing).

## License
[CC 4.0 International](LICENSE).